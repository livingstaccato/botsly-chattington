module gitlab.com/mattress/botsly-chattington

require (
	github.com/golang/mock v1.2.0
	github.com/keybase/go-keybase-chat-bot v0.0.0-20190201210759-d9129cbc7593
	github.com/sirupsen/logrus v1.3.0
	gopkg.in/yaml.v2 v2.2.2
)
