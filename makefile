PROJECT_NAME := "botsly-chattington"
BIN_NAME := "botsly"
PKG := "gitlab.com/mattress/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint updatedep release

all: build

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -v -covermode=count ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./coverage.sh

coverhtml: coverage ## Generate global code coverage report in HTML
	@go tool cover -html=cover/coverage.cov -o cover.html; open cover.html

dep: ## Get the dependencies
	@go get -mod=readonly -v -d ./...

updatedep: ## Get the dependencies
	@go get -u -v -d ./...

build: dep ## Build the binary file
	@go build -mod=readonly -ldflags "-X ${PKG}/version.Version=`git describe --always --long --tags` -X ${PKG}/version.Timestamp=`date -u +%FT%TZ`" -i -v -o ${BIN_NAME} $(PKG)

release: ## Tag master branch with new version
	$(eval old := $(shell git describe --tags --abbrev=0 | sed -Ee 's/^v|-.*//'))
ifeq ($(bump),major)
	$(eval f := 1)
else ifeq ($(bump),minor)
	$(eval f := 2)
else
	$(eval f := 3)
endif
	$(eval v := $(shell echo $(old) | awk -F. -v OFS=. -v f=$(f) '{ $$f++ } 1'))
	@git tag "$v"
	@git push --tags
	$(info "Tagged version $(v)")

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
