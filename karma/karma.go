// Package karma manages fake internet points that have become so popular
// in shitty electron chat apps for various reasons
package karma

import (
	"encoding/json"
	"fmt"
	"github.com/keybase/go-keybase-chat-bot/kbchat"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mattress/botsly-chattington/api"
	"io"
	"io/ioutil"
	"regexp"
	"sort"
	"strings"
	"time"
)

// Karma struct tracks each users karma, and increments or decrements based on user input
type Karma struct {
	counter       karmaMap
	api           api.Kbchatapi
	karmaRegex    *regexp.Regexp
	karmaCmdRegex *regexp.Regexp
	karmaMatch    []string
	filepath      string
	ticker        *time.Ticker
}

// karmaMap is the username to karma count mapping
type karmaMap map[string]int

// NewKarma Returns a new Karma object that manages a list of channel users and their respective karmas
func NewKarma(api api.Kbchatapi, fp string) *Karma {
	var k = &Karma{
		counter: make(karmaMap),
		api:     api,
		// Compile regexp for detecting karma command @usrname1++ or @username2----- etc.
		karmaRegex: regexp.MustCompile("^@([\\d\\w]{3,})\\s*((\\+{2,})|(\\-{2,}))$"),
		// Compile regexp for detecting karma list command /karma
		// TODO implement list sorting
		karmaCmdRegex: regexp.MustCompile("^/karma\\s*(top|bot)*$"),
		filepath:      fp,
		ticker:        time.NewTicker(time.Minute * 5),
	}
	return k
}

// LoadKarmaData reads karma data from supplied Reader
func (k *Karma) LoadKarmaData(r io.Reader) error {
	bytes, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}
	km := karmaMap{}
	json.Unmarshal(bytes, &km)
	k.counter = km
	k.startPersistTimer()
	return nil
}

// startPersistTimer kicks off a timer that triggers persisting karma data to disk
func (k *Karma) startPersistTimer() {
	go func() {
		for range k.ticker.C {
			if err := k.persist(); err != nil {
				log.WithFields(log.Fields{
					"err": err.Error(),
				}).Error("error in persist timer")
			}
		}
	}()
}

// stopPersistTimer stops the persistence timer
func (k *Karma) stopPersistTimer() {
	k.ticker.Stop()
}

// persist writes the karma map to disk
func (k *Karma) persist() error {
	log.Info("Running persist() on karma")
	bytes, err := json.Marshal(k.counter)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(k.filepath, bytes, 0700)
}

// Shutdown stops the persistane ticker and does a final karma save
func (k *Karma) Shutdown() error {
	k.stopPersistTimer()
	return k.persist()
}

// UpdateKarma increments a users karma count
func (k *Karma) UpdateKarma(user string, count int) {
	currentK := k.counter[user]
	k.counter[user] = currentK + count
}

// GetKarma returns the amount of karma a user has
func (k *Karma) GetKarma(username string) int {
	return k.counter[username]
}

// isKarmaModifyReq detects karma incr/decrement commands
func (k *Karma) isKarmaModifyReq(msg string) bool {
	return k.karmaRegex.MatchString(msg)
}

// isKarmaModifyReq detects requests to list karma
func (k *Karma) isKarmaListReq(msg string) bool {
	return k.karmaCmdRegex.MatchString(msg)

}

// KarmaListener checks if a msg contains karma modification or listing commands and executes them. Returns true if it attempted to process a message, so we dont waste time executing other commands on this message
func (k *Karma) KarmaListener(msg kbchat.SubscriptionMessage) bool {
	body := msg.Message.Content.Text.Body
	lreq := k.isKarmaListReq(body)
	mreq := k.isKarmaModifyReq(body)
	if mreq {
		if err := k.ProcessKarmaChange(msg); err != nil {
			log.WithFields(log.Fields{
				"err":         err.Error(),
				"karmastring": body,
			}).Error("failed to process karma")
			return true
		}
		return true
	} else if lreq {
		if err := k.listKarma(msg.Conversation.Channel.Name, msg.Conversation.Channel.TopicName); err != nil {
			log.WithFields(log.Fields{
				"err": err.Error(),
			}).Error("failed to create karma leaderboard")
			return true
		}
		return true
	}
	return false
}

//
func (k *Karma) listKarma(team string, channel string) error {
	const outputHeader string = "Karma List (User / #)\n"
	kl := make(karmaList, len(k.counter))
	i := 0
	for kk, v := range k.counter {
		kl[i] = karmaListItem{kk, v}
		i++
	}
	sort.Sort(sort.Reverse(kl))
	var out = outputHeader
	for i, v := range kl {
		out += fmt.Sprintf("%d.  %s  /  %d \n", i, v.User, v.Karma)
	}
	return k.api.SendMessageByTeamName(team, out, &channel)
}

type karmaList []karmaListItem

type karmaListItem struct {
	User  string
	Karma int
}

func (k karmaList) Len() int           { return len(k) }
func (k karmaList) Less(i, j int) bool { return k[i].Karma < k[j].Karma }
func (k karmaList) Swap(i, j int)      { k[i], k[j] = k[j], k[i] }

// ProcessKarmaChange updates the karma list with any changes in the received message and replys with the users current karma amount
func (k *Karma) ProcessKarmaChange(msg kbchat.SubscriptionMessage) error {
	data := k.karmaRegex.FindStringSubmatch(msg.Message.Content.Text.Body)
	if strings.Contains(data[2], "+") {
		k.UpdateKarma(data[1], len(strings.TrimSpace(data[2]))-1)
	} else {
		k.UpdateKarma(data[1], -(len(strings.TrimSpace(data[2])) - 1))
	}
	log.WithFields(log.Fields{
		"karma": k.counter,
	}).Debug("Current karma map")
	out := fmt.Sprintf("@%s now has %d internet points", data[1], k.GetKarma(data[1]))
	return k.api.SendMessageByTeamName(msg.Conversation.Channel.Name, out, &msg.Conversation.Channel.TopicName)
}
