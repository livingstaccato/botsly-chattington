package version

// Build info
// These get set during linking, using ldflags
// See build target in Makefile
// go build -ldflags '-X version.Version=x.y.z -X version.Timestamp=xxyyzz'
var (
	Version   = "No version included"
	Timestamp = "No timestamp included"
)
