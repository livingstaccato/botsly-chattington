// Package api provides an interface for wrapping the kbchat calls
// so that other packages can be supplied a mock kbchat for testing
package api

import (
	"github.com/keybase/go-keybase-chat-bot/kbchat"
)

// Kbchatapi is interface for kbchat. This should probably live in the kbchat project, and replace references to a concrete type with the interface to aid in testing. However, since these are both experimental projects, I'll leave this here for now, and possible refacot kbchat to use the interface. Or maybe even make a v2 of kbchat that builds the kbchat functionality into botsly-chattington since all kbchat really does is listen/write to a unix socker.
type Kbchatapi interface {
	GetConversations(bool) ([]kbchat.Conversation, error)
	GetTextMessages(kbchat.Channel, bool) ([]kbchat.Message, error)
	SendMessage(kbchat.Channel, string) error
	SendMessageByTlfName(string, string) error
	SendMessageByTeamName(string, string, *string) error
	SendAttachmentByTeam(string, string, string, *string) error
	Username() string
	GetUsername() string
	ListenForNewTextMessages() (kbchat.NewMessageSubscription, error)
}
