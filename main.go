// Botsly Chattington is an experimental Keybase chatbot
package main

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/keybase/go-keybase-chat-bot/kbchat"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mattress/botsly-chattington/commands"
	"gitlab.com/mattress/botsly-chattington/config"
	"gitlab.com/mattress/botsly-chattington/karma"
	"gitlab.com/mattress/botsly-chattington/version"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"
)

// Command line options/config created in init()
var (
	opts     kbchat.RunOptions
	sigs     chan os.Signal
	loglevel string
	logfile  *os.File
	cfgpath  string
)

// Config object contains options loaded from yaml file
var cfg config.Config

func init() {
	// Command line flags
	flag.StringVar(&opts.KeybaseLocation, "keybase", "keybase", "the location of the Keybase app")
	flag.StringVar(&loglevel, "loglvl", "info", "sets log level. available: ['info', 'debug']")
	flag.StringVar(&cfgpath, "cfgpath", "/opt/botsly/config.yml", "path to yaml config file")
	flag.Parse()

	initConfig()
	initLogging(loglevel, cfg.LogFile)
	log.WithFields(log.Fields{
		"VERSION":   version.Version,
		"BUILDTIME": version.Timestamp,
	}).Info("###### Starting Botsly Chattington")

	// Listen for interrupts to gracefully shutdown
	sigs = make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

}

func main() {
	// Read app config

	// Start the chat api
	kbc, err := kbchat.Start(opts)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Fatal("Error creating keybase API, aborting")
	}
	// initialize commands
	cmd := commands.NewCommandManager(kbc, &cfg)
	// initialize karma struct
	ks := karma.NewKarma(kbc, cfg.KarmaStoreFile)
	// load karma from store
	kb, err := ioutil.ReadFile(cfg.KarmaStoreFile)
	if err != nil {
		log.WithFields(log.Fields{
			"err":      err.Error(),
			"filepath": cfg.KarmaStoreFile,
		}).Fatal("error opening karma file")
	}
	ks.LoadKarmaData(bytes.NewBuffer(kb))

	// Gracefully shutdown when SIGINT|SIGTERM recvd
	handleSignals(ks, logfile)

	// Create a new subscription to listen for messages
	sub, err := kbc.ListenForNewTextMessages()
	if err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Fatal("failed getting subscription")
	}

	// Main event loop handling incoming messages
	log.Info("###### Botsly Chattington Online ######")
	for {
		var msg kbchat.SubscriptionMessage
		// Retrieve new message
		msg, err = sub.Read()
		if err != nil {
			log.WithFields(log.Fields{
				"err": err.Error(),
			}).Error("failed to read next message")
			continue
		}

		// Check if the message content has a sequence of >1 -/+ or
		// if it is a request to view karma
		if ks.KarmaListener(msg) {
			continue
		}
		// Check if the message starts with cmd prefix
		if ex := cmd.CommandListener(msg); ex != nil {
			if err := cmd.Execute(*ex); err != nil {
				log.WithFields(log.Fields{
					"err": err.Error(),
				}).Error("failed to exec command")
				continue
			}
		}
	}
}

func initConfig() {
	bytes, err := ioutil.ReadFile(cfgpath)
	if err != nil {
		log.WithFields(log.Fields{
			"err":     err.Error(),
			"cfgpath": cfgpath,
		}).Fatal("couldnt open config file")
	}

	err = yaml.Unmarshal(bytes, &cfg)
	if err != nil {
		log.WithFields(log.Fields{
			"err":   err.Error(),
			"bytes": bytes,
		}).Fatal("error unmarshaling config")
	}
	cfg.Validate()
}

// handleSignals launches a goroutine that listens for SIGTERM/SIGKILL and
// gracefully shut the bot down
func handleSignals(ks *karma.Karma, logfile *os.File) {
	go func(ks *karma.Karma, l *os.File) {
		sig := <-sigs
		log.WithFields(log.Fields{
			"signal": sig,
		}).Info("Signal Received")

		if err := ks.Shutdown(); err != nil {
			log.WithFields(log.Fields{
				"err": err.Error(),
			}).Fatal("failed shutting down gracefully")
		}
		log.Info("###### Shutdown complete")
		if err := logfile.Close(); err != nil {
			fmt.Printf("ERROR CLOSING LOG FILE %s", err.Error())
		}
		os.Exit(0)
	}(ks, logfile)
}

// initLogging opens the logfile and configures logrus
func initLogging(l string, logpath string) {
	switch l {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	}

	logfile, err := os.OpenFile(logpath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0660)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Fatal("Error opening log file")
	}
	log.SetOutput(logfile)
}
