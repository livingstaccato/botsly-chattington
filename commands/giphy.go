package commands

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"time"
)

const giphyBaseURL string = "https://api.giphy.com"
const giphySearchURI string = "/v1/gifs/search"

// GiphyCmd is the main struct fir the the giphy image search command
type GiphyCmd struct {
	apiKey       string
	gifDirectory string
}

// NewGiphyCmd returns an instance of GiphyCmd with populated api key
func NewGiphyCmd(key string, path string) *GiphyCmd {
	return &GiphyCmd{apiKey: key, gifDirectory: path}
}

type giphyResponse struct {
	Data []giphyData `json:"data"`
}

type giphyData struct {
	URL      string  `json:"url"`
	Score    float64 `json:"_score"`
	EmbedURL string  `json:"embed_url"`
	ID       string  `json:"id"`
}

// HandleCmd is the handler for GiphyCmd
// Example: /gif kitties
// This handler takes user supplied string and queries the giphy database to return a gif
//  - args will contain everything after the '/gif' part of the issued command
func (e *GiphyCmd) HandleCmd(cmd Command) error {
	c := &http.Client{
		Timeout: time.Second * 20,
	}

	req, err := http.NewRequest(http.MethodGet, giphyBaseURL+giphySearchURI, nil)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Debug("error creating giphy request")
		return err
	}

	q := req.URL.Query()
	q.Add("api_key", e.apiKey)
	q.Add("q", cmd.CmdArgs[1])
	q.Add("limit", "50")
	req.URL.RawQuery = q.Encode()
	log.WithFields(log.Fields{
		"query": q,
	}).Debug("Gipy request params")

	res, err := c.Do(req)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Debug("error GETing giphy request")
		return err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Debug("error reading giphy response bodies")
		return err
	}
	var gr giphyResponse
	if err := json.Unmarshal(body, &gr); err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Debug("error unmarshaling json giphy response")
		return err
	}

	// Query returned no results
	if len(gr.Data) == 0 {
		log.WithFields(log.Fields{
			"query": cmd.CmdArgs[1],
		}).Info("no gifs found for query")
		return cmd.SendMessage("get better at search terms nub")
	}

	// Randomly choose an image from the result set and download the image to local fs
	r := rand.Intn(len(gr.Data))
	url := fmt.Sprintf("https://media.giphy.com/media/%s/giphy.gif", gr.Data[r].ID)
	req, err = http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.WithFields(log.Fields{
			"err": err.Error(),
		}).Debug("error creating giphy request")
		return err
	}

	// Create an empty file on local fs
	filename := fmt.Sprintf("%s/%s.giphy.gif", e.gifDirectory, gr.Data[r].ID)
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	// Download
	res, err = c.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// Write it
	_, err = io.Copy(f, res.Body)
	if err != nil {
		return err
	}

	// Post to keybase
	err = cmd.SendAttachment(filename, "")
	if err != nil {
		return err
	}
	return nil
}
