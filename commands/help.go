package commands

import (
	"fmt"
	"gitlab.com/mattress/botsly-chattington/version"
)

// Help struct is the data structure for the Help Command
type Help struct {
	output string
}

var head string = `Botsly Chattington III, Esq. at your service. v` + version.Version + `
 Use me:
 - Commands
`

const foot = ` - Karma
    username++ or username-- ### minimum 2 +/- to trigger karma
`

// HandleCmd processes requests for help
func (h *Help) HandleCmd(cmd Command) error {
	return cmd.SendMessage(head + h.output + foot)
}

// NewHelpCmd returns a pointer a new Help object populated with a list of available commands
func NewHelpCmd(cmds []string) *Help {
	var out string
	for _, v := range cmds {
		out += fmt.Sprintf("      /%s\n", v)
	}
	return &Help{output: out}
}
